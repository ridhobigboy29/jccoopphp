<?php

require_once ("animals.php");

class frog extends animals{
    public $name = "Frog";
    public $legs = 4;
    public $cold_Blooded = "no";
    public $sound = "Hop Hop";
}

?>